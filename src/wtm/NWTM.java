package wtm;

import wtm.parser.NWTMParser;

public class NWTM {
	public static final int CODESIZE = 2500;
    public static final int MEMSIZE = 10000;
    
    public final static int REGNUM = 6;
	
	public final static int A0 = 0;
	public final static int T1 = 1;
	public final static int FP = 2;
	public final static int AL = 3;
	public final static int SP = 4;
	public final static int RA = 5;
	
	private int steps = 0;
 
    private NWTMInstruction[] code;
    private int ip;
    
    private int[] memory;
    private int[] registers;
    
    public NWTM(NWTMInstruction[] code) {
    	//init code and instruction pointer
    	this.code = code;
    	ip = 0;
    	
    	//init empty memory
    	memory = new int[MEMSIZE];

    	//init registers
    	registers = new int[REGNUM];
    	registers[SP] = MEMSIZE - 1; //starts from end of memory, although this location is NEVER used (see push)
    	registers[FP] = MEMSIZE - 2; //points to the bottom-most usable memory location
    }
    
    public void execute() {
    	while(true) {
    		if (registers[SP] <= 0) {
    			System.out.println("NWTM Error: out of memory.");
    			return;
    		}
    		if (ip >= code.length) {
    			System.out.println("Halted.");
    			return;
    		}
    		
    		NWTMInstruction instruction = code[ip];
    		
    		switch(instruction.instr) {
    			case NWTMParser.PUSH:	//push $r1
    				push(registers[instruction.arg0]);
    				break;
    			case NWTMParser.TOP:	//top $r1
    				registers[instruction.arg0] = top();
    				break;
    			case NWTMParser.POP:	//pop
    				pop();
    				break;
    			case NWTMParser.MOVE:	//move $r1 $r2
    				registers[instruction.arg0] = registers[instruction.arg1];
    				break;
    			case NWTMParser.ADD:	//add $r1 $r2 $r3
    				registers[instruction.arg0] = registers[instruction.arg1] + registers[instruction.arg2];
    				break;
    			case NWTMParser.ADDINT:	//add $r1 $r2 val
    				registers[instruction.arg0] = registers[instruction.arg1] + instruction.arg2;
    				break;
    			case NWTMParser.SUB:	//sub $r1 $r2 $r3
    				registers[instruction.arg0] = registers[instruction.arg1] - registers[instruction.arg2];
    				break;
    			case NWTMParser.MULT:	//mult $r1 $r2 $r3
    				registers[instruction.arg0] = registers[instruction.arg1] * registers[instruction.arg2];
    				break;
    			case NWTMParser.DIV:	//div $r1 $r2 $r3
    				registers[instruction.arg0] = registers[instruction.arg1] / registers[instruction.arg2];
    				break;
    			case NWTMParser.MOD:	//mod $r1 $r2 $r3
    				registers[instruction.arg0] = registers[instruction.arg1] % registers[instruction.arg2];
    				break;
    			case NWTMParser.CHANGESIGN:	//csig $r1 $r2
    				registers[instruction.arg0] = -registers[instruction.arg1];
    				break;
    			case NWTMParser.NEGATE:	//not $r1 $r2
    				registers[instruction.arg0] = 1-registers[instruction.arg1];
    				break;
    			case NWTMParser.STOREW:	//sw $r1 offset($r2)
    				memory[instruction.arg1 + registers[instruction.arg2]] = registers[instruction.arg0];
    				break;
    			case NWTMParser.LOADW:	//lw $r1 offset($r2)
    				registers[instruction.arg0] = memory[instruction.arg1 + registers[instruction.arg2]];
    				break;
    			case NWTMParser.LOADINT:	//li $r1 val
    				registers[instruction.arg0] = instruction.arg1;
    				break;
    			case NWTMParser.BRANCH:	//b address
    				ip = instruction.arg2-1;
    				break;
    			case NWTMParser.BRANCHEQ:	//beq $r1 $r2 address
    				if (registers[instruction.arg0] == registers[instruction.arg1]) ip = instruction.arg2-1;
    				break;
    			case NWTMParser.BRANCHLESSEQ:	//bleq $r1 $r2 address
    				if (registers[instruction.arg0] <= registers[instruction.arg1]) ip = instruction.arg2-1;
    				break;
    			case NWTMParser.JUMPANDLINK:	//jal address
    				registers[RA] = ip+1;
    				ip = instruction.arg2-1;
    				break;
    			case NWTMParser.JUMPREGISTER:	//jr $r1
    				ip = registers[instruction.arg0]-1;
    				break;
    			case NWTMParser.PRINT:	//print $r1
    				System.out.println(registers[instruction.arg0]);
    				break;
    			case NWTMParser.HALT:	//halt
    				System.out.println("Halted.");
    				return;
    		}
    		
    		ip++;
    		
    		//easter egg
    		steps++;
    		if(steps == 1000000000) System.out.println("NWTM warning: Looks like it's turtles all the way down...");
    	}
    }
    
    private void push(int value) {
    	memory[--registers[SP]] = value;
    }
    
    private int top() {
    	return memory[registers[SP]];
    }
    
    private void pop() {
    	//Inefficient, consider ignoring check
    	registers[SP] = registers[SP]+1 > MEMSIZE ? MEMSIZE : registers[SP]+1;
    }
}
