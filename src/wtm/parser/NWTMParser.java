// Generated from NWTM.g4 by ANTLR 4.6
package wtm.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class NWTMParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PUSH=1, TOP=2, POP=3, MOVE=4, ADD=5, ADDINT=6, SUB=7, MULT=8, DIV=9, MOD=10, 
		CHANGESIGN=11, NEGATE=12, STOREW=13, LOADW=14, LOADINT=15, BRANCH=16, 
		BRANCHEQ=17, BRANCHLESSEQ=18, JUMPANDLINK=19, JUMPREGISTER=20, PRINT=21, 
		HALT=22, LPAR=23, RPAR=24, COL=25, LABEL=26, NUMBER=27, REGISTER=28, WHITESP=29, 
		ERR=30;
	public static final int
		RULE_assembly = 0, RULE_instruction = 1;
	public static final String[] ruleNames = {
		"assembly", "instruction"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'push'", "'top'", "'pop'", "'move'", "'add'", "'addi'", "'sub'", 
		"'mult'", "'div'", "'mod'", "'csig'", "'not'", "'sw'", "'lw'", "'li'", 
		"'b'", "'beq'", "'bleq'", "'jal'", "'jr'", "'print'", "'halt'", "'('", 
		"')'", "':'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "PUSH", "TOP", "POP", "MOVE", "ADD", "ADDINT", "SUB", "MULT", "DIV", 
		"MOD", "CHANGESIGN", "NEGATE", "STOREW", "LOADW", "LOADINT", "BRANCH", 
		"BRANCHEQ", "BRANCHLESSEQ", "JUMPANDLINK", "JUMPREGISTER", "PRINT", "HALT", 
		"LPAR", "RPAR", "COL", "LABEL", "NUMBER", "REGISTER", "WHITESP", "ERR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "NWTM.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public NWTMParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class AssemblyContext extends ParserRuleContext {
		public List<InstructionContext> instruction() {
			return getRuleContexts(InstructionContext.class);
		}
		public InstructionContext instruction(int i) {
			return getRuleContext(InstructionContext.class,i);
		}
		public AssemblyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assembly; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NWTMVisitor ) return ((NWTMVisitor<? extends T>)visitor).visitAssembly(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssemblyContext assembly() throws RecognitionException {
		AssemblyContext _localctx = new AssemblyContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_assembly);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(7);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PUSH) | (1L << TOP) | (1L << POP) | (1L << MOVE) | (1L << ADD) | (1L << ADDINT) | (1L << SUB) | (1L << MULT) | (1L << DIV) | (1L << MOD) | (1L << CHANGESIGN) | (1L << NEGATE) | (1L << STOREW) | (1L << LOADW) | (1L << LOADINT) | (1L << BRANCH) | (1L << BRANCHEQ) | (1L << BRANCHLESSEQ) | (1L << JUMPANDLINK) | (1L << JUMPREGISTER) | (1L << PRINT) | (1L << HALT) | (1L << LABEL))) != 0)) {
				{
				{
				setState(4);
				instruction();
				}
				}
				setState(9);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstructionContext extends ParserRuleContext {
		public Token register;
		public Token dest;
		public Token src;
		public Token left;
		public Token right;
		public Token value;
		public Token offset;
		public Token label;
		public TerminalNode PUSH() { return getToken(NWTMParser.PUSH, 0); }
		public List<TerminalNode> REGISTER() { return getTokens(NWTMParser.REGISTER); }
		public TerminalNode REGISTER(int i) {
			return getToken(NWTMParser.REGISTER, i);
		}
		public TerminalNode TOP() { return getToken(NWTMParser.TOP, 0); }
		public TerminalNode POP() { return getToken(NWTMParser.POP, 0); }
		public TerminalNode MOVE() { return getToken(NWTMParser.MOVE, 0); }
		public TerminalNode ADD() { return getToken(NWTMParser.ADD, 0); }
		public TerminalNode ADDINT() { return getToken(NWTMParser.ADDINT, 0); }
		public TerminalNode NUMBER() { return getToken(NWTMParser.NUMBER, 0); }
		public TerminalNode SUB() { return getToken(NWTMParser.SUB, 0); }
		public TerminalNode MULT() { return getToken(NWTMParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(NWTMParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(NWTMParser.MOD, 0); }
		public TerminalNode CHANGESIGN() { return getToken(NWTMParser.CHANGESIGN, 0); }
		public TerminalNode NEGATE() { return getToken(NWTMParser.NEGATE, 0); }
		public TerminalNode STOREW() { return getToken(NWTMParser.STOREW, 0); }
		public TerminalNode LPAR() { return getToken(NWTMParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(NWTMParser.RPAR, 0); }
		public TerminalNode LOADW() { return getToken(NWTMParser.LOADW, 0); }
		public TerminalNode LOADINT() { return getToken(NWTMParser.LOADINT, 0); }
		public TerminalNode COL() { return getToken(NWTMParser.COL, 0); }
		public TerminalNode LABEL() { return getToken(NWTMParser.LABEL, 0); }
		public TerminalNode BRANCH() { return getToken(NWTMParser.BRANCH, 0); }
		public TerminalNode BRANCHEQ() { return getToken(NWTMParser.BRANCHEQ, 0); }
		public TerminalNode BRANCHLESSEQ() { return getToken(NWTMParser.BRANCHLESSEQ, 0); }
		public TerminalNode JUMPANDLINK() { return getToken(NWTMParser.JUMPANDLINK, 0); }
		public TerminalNode JUMPREGISTER() { return getToken(NWTMParser.JUMPREGISTER, 0); }
		public TerminalNode PRINT() { return getToken(NWTMParser.PRINT, 0); }
		public TerminalNode HALT() { return getToken(NWTMParser.HALT, 0); }
		public InstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instruction; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NWTMVisitor ) return ((NWTMVisitor<? extends T>)visitor).visitInstruction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstructionContext instruction() throws RecognitionException {
		InstructionContext _localctx = new InstructionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_instruction);
		try {
			setState(82);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PUSH:
				enterOuterAlt(_localctx, 1);
				{
				setState(10);
				match(PUSH);
				setState(11);
				((InstructionContext)_localctx).register = match(REGISTER);
				}
				break;
			case TOP:
				enterOuterAlt(_localctx, 2);
				{
				setState(12);
				match(TOP);
				setState(13);
				((InstructionContext)_localctx).register = match(REGISTER);
				}
				break;
			case POP:
				enterOuterAlt(_localctx, 3);
				{
				setState(14);
				match(POP);
				}
				break;
			case MOVE:
				enterOuterAlt(_localctx, 4);
				{
				setState(15);
				match(MOVE);
				setState(16);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(17);
				((InstructionContext)_localctx).src = match(REGISTER);
				}
				break;
			case ADD:
				enterOuterAlt(_localctx, 5);
				{
				setState(18);
				match(ADD);
				setState(19);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(20);
				((InstructionContext)_localctx).left = match(REGISTER);
				setState(21);
				((InstructionContext)_localctx).right = match(REGISTER);
				}
				break;
			case ADDINT:
				enterOuterAlt(_localctx, 6);
				{
				setState(22);
				match(ADDINT);
				setState(23);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(24);
				((InstructionContext)_localctx).left = match(REGISTER);
				setState(25);
				((InstructionContext)_localctx).value = match(NUMBER);
				}
				break;
			case SUB:
				enterOuterAlt(_localctx, 7);
				{
				setState(26);
				match(SUB);
				setState(27);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(28);
				((InstructionContext)_localctx).left = match(REGISTER);
				setState(29);
				((InstructionContext)_localctx).right = match(REGISTER);
				}
				break;
			case MULT:
				enterOuterAlt(_localctx, 8);
				{
				setState(30);
				match(MULT);
				setState(31);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(32);
				((InstructionContext)_localctx).left = match(REGISTER);
				setState(33);
				((InstructionContext)_localctx).right = match(REGISTER);
				}
				break;
			case DIV:
				enterOuterAlt(_localctx, 9);
				{
				setState(34);
				match(DIV);
				setState(35);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(36);
				((InstructionContext)_localctx).left = match(REGISTER);
				setState(37);
				((InstructionContext)_localctx).right = match(REGISTER);
				}
				break;
			case MOD:
				enterOuterAlt(_localctx, 10);
				{
				setState(38);
				match(MOD);
				setState(39);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(40);
				((InstructionContext)_localctx).left = match(REGISTER);
				setState(41);
				((InstructionContext)_localctx).right = match(REGISTER);
				}
				break;
			case CHANGESIGN:
				enterOuterAlt(_localctx, 11);
				{
				setState(42);
				match(CHANGESIGN);
				setState(43);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(44);
				((InstructionContext)_localctx).src = match(REGISTER);
				}
				break;
			case NEGATE:
				enterOuterAlt(_localctx, 12);
				{
				setState(45);
				match(NEGATE);
				setState(46);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(47);
				((InstructionContext)_localctx).src = match(REGISTER);
				}
				break;
			case STOREW:
				enterOuterAlt(_localctx, 13);
				{
				setState(48);
				match(STOREW);
				setState(49);
				((InstructionContext)_localctx).src = match(REGISTER);
				setState(50);
				((InstructionContext)_localctx).offset = match(NUMBER);
				setState(51);
				match(LPAR);
				setState(52);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(53);
				match(RPAR);
				}
				break;
			case LOADW:
				enterOuterAlt(_localctx, 14);
				{
				setState(54);
				match(LOADW);
				setState(55);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(56);
				((InstructionContext)_localctx).offset = match(NUMBER);
				setState(57);
				match(LPAR);
				setState(58);
				((InstructionContext)_localctx).src = match(REGISTER);
				setState(59);
				match(RPAR);
				}
				break;
			case LOADINT:
				enterOuterAlt(_localctx, 15);
				{
				setState(60);
				match(LOADINT);
				setState(61);
				((InstructionContext)_localctx).dest = match(REGISTER);
				setState(62);
				((InstructionContext)_localctx).value = match(NUMBER);
				}
				break;
			case LABEL:
				enterOuterAlt(_localctx, 16);
				{
				setState(63);
				((InstructionContext)_localctx).label = match(LABEL);
				setState(64);
				match(COL);
				}
				break;
			case BRANCH:
				enterOuterAlt(_localctx, 17);
				{
				setState(65);
				match(BRANCH);
				setState(66);
				((InstructionContext)_localctx).label = match(LABEL);
				}
				break;
			case BRANCHEQ:
				enterOuterAlt(_localctx, 18);
				{
				setState(67);
				match(BRANCHEQ);
				setState(68);
				((InstructionContext)_localctx).left = match(REGISTER);
				setState(69);
				((InstructionContext)_localctx).right = match(REGISTER);
				setState(70);
				((InstructionContext)_localctx).label = match(LABEL);
				}
				break;
			case BRANCHLESSEQ:
				enterOuterAlt(_localctx, 19);
				{
				setState(71);
				match(BRANCHLESSEQ);
				setState(72);
				((InstructionContext)_localctx).left = match(REGISTER);
				setState(73);
				((InstructionContext)_localctx).right = match(REGISTER);
				setState(74);
				((InstructionContext)_localctx).label = match(LABEL);
				}
				break;
			case JUMPANDLINK:
				enterOuterAlt(_localctx, 20);
				{
				setState(75);
				match(JUMPANDLINK);
				setState(76);
				((InstructionContext)_localctx).label = match(LABEL);
				}
				break;
			case JUMPREGISTER:
				enterOuterAlt(_localctx, 21);
				{
				setState(77);
				match(JUMPREGISTER);
				setState(78);
				((InstructionContext)_localctx).register = match(REGISTER);
				}
				break;
			case PRINT:
				enterOuterAlt(_localctx, 22);
				{
				setState(79);
				match(PRINT);
				setState(80);
				((InstructionContext)_localctx).register = match(REGISTER);
				}
				break;
			case HALT:
				enterOuterAlt(_localctx, 23);
				{
				setState(81);
				match(HALT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3 W\4\2\t\2\4\3\t\3"+
		"\3\2\7\2\b\n\2\f\2\16\2\13\13\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3U\n\3\3\3\2\2\4\2\4\2\2k\2"+
		"\t\3\2\2\2\4T\3\2\2\2\6\b\5\4\3\2\7\6\3\2\2\2\b\13\3\2\2\2\t\7\3\2\2\2"+
		"\t\n\3\2\2\2\n\3\3\2\2\2\13\t\3\2\2\2\f\r\7\3\2\2\rU\7\36\2\2\16\17\7"+
		"\4\2\2\17U\7\36\2\2\20U\7\5\2\2\21\22\7\6\2\2\22\23\7\36\2\2\23U\7\36"+
		"\2\2\24\25\7\7\2\2\25\26\7\36\2\2\26\27\7\36\2\2\27U\7\36\2\2\30\31\7"+
		"\b\2\2\31\32\7\36\2\2\32\33\7\36\2\2\33U\7\35\2\2\34\35\7\t\2\2\35\36"+
		"\7\36\2\2\36\37\7\36\2\2\37U\7\36\2\2 !\7\n\2\2!\"\7\36\2\2\"#\7\36\2"+
		"\2#U\7\36\2\2$%\7\13\2\2%&\7\36\2\2&\'\7\36\2\2\'U\7\36\2\2()\7\f\2\2"+
		")*\7\36\2\2*+\7\36\2\2+U\7\36\2\2,-\7\r\2\2-.\7\36\2\2.U\7\36\2\2/\60"+
		"\7\16\2\2\60\61\7\36\2\2\61U\7\36\2\2\62\63\7\17\2\2\63\64\7\36\2\2\64"+
		"\65\7\35\2\2\65\66\7\31\2\2\66\67\7\36\2\2\67U\7\32\2\289\7\20\2\29:\7"+
		"\36\2\2:;\7\35\2\2;<\7\31\2\2<=\7\36\2\2=U\7\32\2\2>?\7\21\2\2?@\7\36"+
		"\2\2@U\7\35\2\2AB\7\34\2\2BU\7\33\2\2CD\7\22\2\2DU\7\34\2\2EF\7\23\2\2"+
		"FG\7\36\2\2GH\7\36\2\2HU\7\34\2\2IJ\7\24\2\2JK\7\36\2\2KL\7\36\2\2LU\7"+
		"\34\2\2MN\7\25\2\2NU\7\34\2\2OP\7\26\2\2PU\7\36\2\2QR\7\27\2\2RU\7\36"+
		"\2\2SU\7\30\2\2T\f\3\2\2\2T\16\3\2\2\2T\20\3\2\2\2T\21\3\2\2\2T\24\3\2"+
		"\2\2T\30\3\2\2\2T\34\3\2\2\2T \3\2\2\2T$\3\2\2\2T(\3\2\2\2T,\3\2\2\2T"+
		"/\3\2\2\2T\62\3\2\2\2T8\3\2\2\2T>\3\2\2\2TA\3\2\2\2TC\3\2\2\2TE\3\2\2"+
		"\2TI\3\2\2\2TM\3\2\2\2TO\3\2\2\2TQ\3\2\2\2TS\3\2\2\2U\5\3\2\2\2\4\tT";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}