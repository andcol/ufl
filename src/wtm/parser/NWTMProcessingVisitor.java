package wtm.parser;

import java.util.HashMap;

import wtm.NWTM;
import wtm.NWTMInstruction;
import wtm.parser.NWTMParser.AssemblyContext;
import wtm.parser.NWTMParser.InstructionContext;

public class NWTMProcessingVisitor extends NWTMBaseVisitor<Void> {

	public NWTMInstruction[] code = new NWTMInstruction[NWTM.CODESIZE];    
    private int ip = 0;
    private boolean tooMuchCode = false;
    private HashMap<String,Integer> labelAddresses = new HashMap<String,Integer>();
    private HashMap<Integer,String> labelReferences = new HashMap<Integer,String>();
    
	@Override
	public Void visitAssembly(AssemblyContext ctx) {
		visitChildren(ctx);
    	for (Integer referenceAddress: labelReferences.keySet()) {
            code[referenceAddress].arg2 = labelAddresses.get(labelReferences.get(referenceAddress));
    	}
    	if(ip < NWTM.CODESIZE) {
    		code[ip] = new NWTMInstruction(NWTMParser.HALT); //Add a cautionary halt
    	}
    	return null;
	}

	@Override
	public Void visitInstruction(InstructionContext ctx) {
		if (ip >= NWTM.CODESIZE) {
			tooMuchCode = true;
			return null;
		}
		int type = ctx.getStart().getType();
		switch(type) {
			case NWTMParser.PUSH:	//push $r1
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.register.getText()));
				break;
			case NWTMParser.TOP:	//top $r1
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.register.getText()));
				break;
			case NWTMParser.POP:	//pop
				code[ip++] = new NWTMInstruction(type);
				break;
			case NWTMParser.MOVE:	//move $r1 $r2
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.src.getText()));
				break;
			case NWTMParser.ADD:	//add $r1 $r2 $r3
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.left.getText()), tokenizeRegister(ctx.right.getText()));
				break;
			case NWTMParser.ADDINT:	//add $r1 $r2 val
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.left.getText()), Integer.parseInt(ctx.value.getText()));
				break;
			case NWTMParser.SUB:	//sub $r1 $r2 $r3
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.left.getText()), tokenizeRegister(ctx.right.getText()));
				break;
			case NWTMParser.MULT:	//mult $r1 $r2 $r3
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.left.getText()), tokenizeRegister(ctx.right.getText()));
				break;
			case NWTMParser.DIV:	//div $r1 $r2 $r3
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.left.getText()), tokenizeRegister(ctx.right.getText()));
				break;
			case NWTMParser.MOD:	//mod $r1 $r2 $r3
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.left.getText()), tokenizeRegister(ctx.right.getText()));
				break;
			case NWTMParser.CHANGESIGN:	//csig $r1 $r2
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.src.getText()));
				break;
			case NWTMParser.NEGATE:	//not $r1 $r2
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), tokenizeRegister(ctx.src.getText()));
				break;
			case NWTMParser.STOREW:	//sw $r1 offset($r2)
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.src.getText()), Integer.parseInt(ctx.offset.getText()), tokenizeRegister(ctx.dest.getText()));
				break;
			case NWTMParser.LOADW:	//lw $r1 offset($r2)
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), Integer.parseInt(ctx.offset.getText()), tokenizeRegister(ctx.src.getText()));
				break;
			case NWTMParser.LOADINT:	//li $r1 val
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.dest.getText()), Integer.parseInt(ctx.value.getText()));
				break;
			case NWTMParser.LABEL:	//label:
				labelAddresses.put(ctx.label.getText(), ip);
				break;
			case NWTMParser.BRANCH:	//b label
				labelReferences.put(ip, ctx.label.getText());
				code[ip++] = new NWTMInstruction(type); //might have to add label
				break;
			case NWTMParser.BRANCHEQ:	//beq $r1 $r2 label
				labelReferences.put(ip, ctx.label.getText());
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.left.getText()), tokenizeRegister(ctx.right.getText())); //might have to add label
				break;
			case NWTMParser.BRANCHLESSEQ:	//bleq $r1 $r2 label
				labelReferences.put(ip, ctx.label.getText());
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.left.getText()), tokenizeRegister(ctx.right.getText())); //might have to add label
				break;
			case NWTMParser.JUMPANDLINK:	//jal label
				labelReferences.put(ip, ctx.label.getText());
				code[ip++] = new NWTMInstruction(type); //might have to add label
				break;
			case NWTMParser.JUMPREGISTER:	//jr $r1
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.register.getText()));
				break;
			case NWTMParser.PRINT:	//print $r1
				code[ip++] = new NWTMInstruction(type, tokenizeRegister(ctx.register.getText()));
				break;
			case NWTMParser.HALT:	//halt
				code[ip++] = new NWTMInstruction(type);
				break;
		}
		
		return null;
	}
	
	private int tokenizeRegister(String regName) {
		if (regName.equals("$a0")) {
			return NWTM.A0;
		} else if (regName.equals("$t1")) {
			return NWTM.T1;
		} else if (regName.equals("$fp")) {
			return NWTM.FP;
		} else if (regName.equals("$al")) {
			return NWTM.AL;
		} else if (regName.equals("$sp")) {
			return NWTM.SP;
		} else if (regName.equals("$ra")) {
			return NWTM.RA;
		} else {
			return -1;
		}
	}
	
	public boolean isTooMuchCode() {
		return tooMuchCode;
	}

}
