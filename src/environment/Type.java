package environment;

public enum Type {
	INT {
		public String toString() {
			return "int";
		}
	}, 
	BOOL {
		public String toString() {
			return "bool";
		}
	},
	VOID {
		public String toString() {
			return "void";
		}
	},
	UNDEFINED {
		public String toString() {
			return "undefined";
		}
	};
	
	public static Type fromString(String string) {
		if(string.equals("int")) {
			return INT;
		} else if(string.equals("bool")) {
			return BOOL;
		}
		return UNDEFINED;
	}
}
