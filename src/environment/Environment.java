package environment;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

public class Environment {
	
	private List<Hashtable<String,SymbolTableEntry>> symbolTable;
	private int currentDepth;
	private int labelNum;
	private List<Integer> localVarNums;
	
	public Environment() {
		symbolTable = new LinkedList<Hashtable<String, SymbolTableEntry>>();
		currentDepth = -1;
		labelNum = -1;
		localVarNums = new LinkedList<Integer>();
	}
	
	public void openScope() {
		Hashtable<String,SymbolTableEntry> scope = new Hashtable<String,SymbolTableEntry>();
		symbolTable.add(0, scope);
		currentDepth++;
		localVarNums.add(0,2);
	}
	
	public void closeScope() {
		symbolTable.remove(0);
		currentDepth--;
		localVarNums.remove(0);
	}
	
	
	
	public boolean addVariable(String id, Type type) {
		if(symbolTable.get(0).containsKey(id)) {
			return false;
		}
		
		VariableSymbolTableEntry newEntry = new VariableSymbolTableEntry(id, currentDepth, type, -localVarNums.get(0));
		symbolTable.get(0).put(id, newEntry);
		localVarNums.set(0, localVarNums.get(0) + 1);
		return true;
	}
	
	public void addParameter(String id, int number) {	//Used for code generation
		VariableSymbolTableEntry newEntry = new VariableSymbolTableEntry(id, currentDepth, null, number+1);
		symbolTable.get(0).put(id, newEntry);
	}
	
	public boolean addFunction(String id, List<String> parameterNames, List<Type> domain, Type codomain) {
		if(symbolTable.get(0).containsKey(id)) {
			return false;
		}
		
		FunctionSymbolTableEntry newEntry = new FunctionSymbolTableEntry(id, currentDepth, parameterNames, domain, codomain, newLabel());
		symbolTable.get(0).put(id, newEntry);
		return true;	
	}
	
	public VariableSymbolTableEntry retrieveVariable(String id) {
		for(int i = 0; i <= currentDepth; i++) {
			if (symbolTable.get(i).containsKey(id)) {
				SymbolTableEntry entry = symbolTable.get(i).get(id);
				if(entry instanceof VariableSymbolTableEntry) {
					return (VariableSymbolTableEntry) entry;
				}
			}
		}
		return null;
	}
	
	public FunctionSymbolTableEntry retrieveFunction(String id) {
		for(int i = 0; i <= currentDepth; i++) {
			if (symbolTable.get(i).containsKey(id)) {
				SymbolTableEntry entry = symbolTable.get(i).get(id);
				if(entry instanceof FunctionSymbolTableEntry) {
					return (FunctionSymbolTableEntry) entry;
				}
			}
		}
		return null;
	}
	
	public int getRelativeDepth(String id) {
		for(int i = 0; i <= currentDepth; i++) {
			if (symbolTable.get(i).containsKey(id)) {
				SymbolTableEntry entry = symbolTable.get(i).get(id);
				if(entry instanceof VariableSymbolTableEntry) {
					return i;
				}
			}
		}
		return -1;
	}
	
	public int getNumberOfLocalVariables() {
		return localVarNums.get(0);
	}

	public String newLabel() {
		labelNum++;
		return "LABEL_" + labelNum;
	}
	
}
