package environment;

public class VariableSymbolTableEntry extends SymbolTableEntry {

	private Type type;
	private int offset;

	public VariableSymbolTableEntry(String id, int depth, Type type, int offset) {
		super(id, depth);
		this.type = type;
		this.offset = offset;
	}

	public Type getType() {
		return type;
	}

	public int getOffset() {
		return offset;
	}
	
}
