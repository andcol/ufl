package environment;

import java.util.List;

public class FunctionSymbolTableEntry extends SymbolTableEntry {
	
	private List<String> parameterNames;
	private List<Type> domain;
	private Type codomain;
	private String label;
	
	public FunctionSymbolTableEntry(String id, int depth, List<String> parameterNames, List<Type> domain, Type codomain, String label) {
		super(id, depth);
		this.parameterNames = parameterNames;
		this.domain = domain;
		this.codomain = codomain;
		this.label = label;
	}

	public List<String> getParameterNames() {
		return parameterNames;
	}

	public List<Type> getDomain() {
		return domain;
	}

	public Type getCodomain() {
		return codomain;
	}

	public String getLabel() {
		return label;
	}
	
}
