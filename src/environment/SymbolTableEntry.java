package environment;

public class SymbolTableEntry {
	
	protected String id;
	protected int depth;
	
	public SymbolTableEntry(String id, int depth) {
		super();
		this.id = id;
		this.depth = depth;
	}
	
	
}
