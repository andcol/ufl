package compiler;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import ast.Program;
import util.Error;
import environment.Environment;
import parser.UFLLexer;
import parser.UFLParser;
import parser.UFLTreeGeneratingVisitor;
import wtm.NWTM;
import wtm.parser.NWTMLexer;
import wtm.parser.NWTMParser;
import wtm.parser.NWTMProcessingVisitor;


public class Compiler {
	
	public static void main(String[] args) throws Exception {
		
		String[] testCases = {"tests/test.usl"};
		
        for (String fileName : testCases) {
        	
        	//ANALYSIS
        	Program ast = analyze(fileName);
        	if(ast == null) {
        		//If code is semantically inconsistent, skip it
        		continue;
        	}
        	
        	//CODE GENERATION
        	String nwtmFilePath = "generated_code/" + fileNameFromPath(fileName) + ".nwtm";
        	if(!compile(ast, nwtmFilePath)) {
        		//If compilation is unsuccessful, skip file
        		continue;
        	}
        	
        	//EXECUTION
        	executeNWTM(nwtmFilePath);
        }
	}
	
	public static Program analyze(String fileName) {
		try {   
        	FileInputStream is = new FileInputStream(fileName);
            ANTLRInputStream input = new ANTLRInputStream(is);
            
            System.out.println("\n*** " + fileName + " ***\n");

			// Create lexer
            UFLLexer lexer = new UFLLexer(input);
            lexer.removeErrorListeners();	//forward errors to parser
			// Create parser
			UFLParser parser = new UFLParser(new CommonTokenStream(lexer));
			parser.setBuildParseTree(true);	// Tell the parser to build the AST
			parser.setErrorHandler(new BailErrorStrategy());	//Throw exception on parsing error
			parser.removeErrorListeners();	//Do not report parsing errors directly to the console
			
			// Build custom visitor
			UFLTreeGeneratingVisitor visitor = new UFLTreeGeneratingVisitor();
			Program ast;
			try {
				ast = (Program)visitor.visit(parser.program());	
			} catch (ParseCancellationException e) {
				System.out.println("Error: could not parse input in " + fileName + ".");
				return null;
			}
			
			boolean correct = true;
			
	        List<Error> semanticErrors = ast.checkSemantics(new Environment());
	        if(semanticErrors.size() > 0) {
	        	correct = false;
	        	for(Error e : semanticErrors) {
	        		System.out.println(e);
	        	}
	        } else {
	        	List<Error> typeErrors = ast.checkType(new Environment());
	        	if(typeErrors.size() > 0) {
	        		correct = false;
	        		for(Error e : typeErrors) {
	        			System.out.println(e);
	        		}
	        	}
	        }
	        
	        if(correct) {
	        	System.out.println("\nProgram is correct and has type " + ast.getType() + ".\n");
	        	return ast;
	        } else {
	        	System.out.println("\nProgram has one ore more semantic issues.\n");
	        	return null;
	        }
	        
		} catch (IOException e) {
			System.out.println("Error: could not read file " + fileName + ".");
			return null;
		}
	}
	
	public static boolean compile(Program ast, String compiledFileName) {
		try {
			String code = ast.generateCode(new Environment()); //generate code string
	        BufferedWriter out = new BufferedWriter(new FileWriter(compiledFileName)); 
	        out.write(code);	//write intermediate code to file
	        out.close();
	        System.out.println("Intermediate code generated in " + compiledFileName + ".\n");
	        return true;
		} catch (IOException e) {
			System.out.println("Error : could not write intermediate code to " + compiledFileName + ".\n");
			return false;
		}
	}
	
	public static void executeNWTM(String fileName) {
		try {
			FileInputStream is = new FileInputStream(fileName);
	        ANTLRInputStream input = new ANTLRInputStream(is);
	        
	        //Build lexer, parser and run visitor on the ast to obtain runnable code
	        NWTMLexer WTMlexer = new NWTMLexer(input);
	        NWTMParser WTMparser = new NWTMParser(new CommonTokenStream(WTMlexer));
	        NWTMProcessingVisitor WTMvisitor = new NWTMProcessingVisitor();
	        WTMvisitor.visit(WTMparser.assembly());
	        
	        //Lexical errors are counted within the lexer
	        if(!WTMlexer.correct) {
	        	System.out.println("Error: there are syntax errors in NWTM file " + fileName + ".");
	        	System.exit(1);
	        }
	        if(WTMvisitor.isTooMuchCode()) {
	        	System.out.println("Error: file " + fileName + " is too long (maximum " + NWTM.CODESIZE + " lines).");
	        	System.exit(1);
	        }
	        
	        System.out.println("Starting New World Turtle Machine (NWTM)...\n");
	        NWTM vm = new NWTM(WTMvisitor.code);
	        vm.execute();	//run the code
		} catch (IOException e) {
			System.out.println("Error: could not read NWTM file " + fileName + ".");
		}
	}
	
	public static String fileNameFromPath(String filePath) {
		String[] splitPath = filePath.split("/");
    	String fileName = splitPath[splitPath.length - 1];
    	fileName = fileName.substring(0, fileName.length() - 4);
    	
    	return fileName;
	}
	
}
