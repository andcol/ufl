// Generated from UFL.g4 by ANTLR 4.6
package parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class UFLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, ROP=23, BOP=24, INTEGER=25, 
		ID=26, WS=27, LINECOMENTS=28, BLOCKCOMENTS=29, ERR=30;
	public static final int
		RULE_program = 0, RULE_declaration = 1, RULE_type = 2, RULE_augexp = 3, 
		RULE_exp = 4, RULE_term = 5, RULE_factor = 6, RULE_value = 7;
	public static final String[] ruleNames = {
		"program", "declaration", "type", "augexp", "exp", "term", "factor", "value"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'compute'", "'='", "'function'", "':'", "'*'", "'->'", "'such that'", 
		"'('", "','", "')'", "'int'", "'bool'", "'where'", "'if'", "'else'", "'-'", 
		"'!'", "'+'", "'/'", "'%'", "'true'", "'false'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, "ROP", 
		"BOP", "INTEGER", "ID", "WS", "LINECOMENTS", "BLOCKCOMENTS", "ERR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "UFL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public UFLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public AugexpContext augexp() {
			return getRuleContext(AugexpContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(16);
			match(T__0);
			setState(17);
			augexp(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
	 
		public DeclarationContext() { }
		public void copyFrom(DeclarationContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FunctionDeclarationContext extends DeclarationContext {
		public Token functionId;
		public TypeContext codomain;
		public Token functionId2;
		public AugexpContext augexp() {
			return getRuleContext(AugexpContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(UFLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(UFLParser.ID, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public FunctionDeclarationContext(DeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitFunctionDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableDeclarationContext extends DeclarationContext {
		public TerminalNode ID() { return getToken(UFLParser.ID, 0); }
		public AugexpContext augexp() {
			return getRuleContext(AugexpContext.class,0);
		}
		public VariableDeclarationContext(DeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitVariableDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declaration);
		int _la;
		try {
			setState(55);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new VariableDeclarationContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(19);
				match(ID);
				setState(20);
				match(T__1);
				setState(21);
				augexp(0);
				}
				break;
			case T__2:
				_localctx = new FunctionDeclarationContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(22);
				match(T__2);
				setState(23);
				((FunctionDeclarationContext)_localctx).functionId = match(ID);
				setState(24);
				match(T__3);
				setState(35);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
				case 1:
					{
					setState(25);
					type();
					setState(30);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__4) {
						{
						{
						setState(26);
						match(T__4);
						setState(27);
						type();
						}
						}
						setState(32);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(33);
					match(T__5);
					}
					break;
				}
				setState(37);
				((FunctionDeclarationContext)_localctx).codomain = type();
				setState(38);
				match(T__6);
				setState(39);
				((FunctionDeclarationContext)_localctx).functionId2 = match(ID);
				setState(40);
				match(T__7);
				setState(49);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ID) {
					{
					setState(41);
					match(ID);
					setState(46);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__8) {
						{
						{
						setState(42);
						match(T__8);
						setState(43);
						match(ID);
						}
						}
						setState(48);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(51);
				match(T__9);
				setState(52);
				match(T__1);
				setState(53);
				augexp(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57);
			_la = _input.LA(1);
			if ( !(_la==T__10 || _la==T__11) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AugexpContext extends ParserRuleContext {
		public AugexpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_augexp; }
	 
		public AugexpContext() { }
		public void copyFrom(AugexpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoundExpressionContext extends AugexpContext {
		public AugexpContext augexp() {
			return getRuleContext(AugexpContext.class,0);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public BoundExpressionContext(AugexpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitBoundExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConditionalExpressionContext extends AugexpContext {
		public AugexpContext thenExp;
		public AugexpContext condition;
		public AugexpContext elseExp;
		public List<AugexpContext> augexp() {
			return getRuleContexts(AugexpContext.class);
		}
		public AugexpContext augexp(int i) {
			return getRuleContext(AugexpContext.class,i);
		}
		public ConditionalExpressionContext(AugexpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitConditionalExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SimpleExpressionContext extends AugexpContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public SimpleExpressionContext(AugexpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitSimpleExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AugexpContext augexp() throws RecognitionException {
		return augexp(0);
	}

	private AugexpContext augexp(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AugexpContext _localctx = new AugexpContext(_ctx, _parentState);
		AugexpContext _prevctx = _localctx;
		int _startState = 6;
		enterRecursionRule(_localctx, 6, RULE_augexp, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new SimpleExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(60);
			exp();
			}
			_ctx.stop = _input.LT(-1);
			setState(80);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(78);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
					case 1:
						{
						_localctx = new ConditionalExpressionContext(new AugexpContext(_parentctx, _parentState));
						((ConditionalExpressionContext)_localctx).thenExp = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_augexp);
						setState(62);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(63);
						match(T__13);
						setState(64);
						((ConditionalExpressionContext)_localctx).condition = augexp(0);
						setState(65);
						match(T__14);
						setState(66);
						((ConditionalExpressionContext)_localctx).elseExp = augexp(3);
						}
						break;
					case 2:
						{
						_localctx = new BoundExpressionContext(new AugexpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_augexp);
						setState(68);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(69);
						match(T__12);
						setState(70);
						declaration();
						setState(75);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(71);
								match(T__8);
								setState(72);
								declaration();
								}
								} 
							}
							setState(77);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
						}
						}
						break;
					}
					} 
				}
				setState(82);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExpContext extends ParserRuleContext {
		public Token minus;
		public Token negation;
		public TermContext left;
		public Token aop;
		public ExpContext right;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpContext exp() throws RecognitionException {
		ExpContext _localctx = new ExpContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__15:
				{
				setState(83);
				((ExpContext)_localctx).minus = match(T__15);
				}
				break;
			case T__16:
				{
				setState(84);
				((ExpContext)_localctx).negation = match(T__16);
				}
				break;
			case T__7:
			case T__20:
			case T__21:
			case INTEGER:
			case ID:
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(87);
			((ExpContext)_localctx).left = term();
			setState(90);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(88);
				((ExpContext)_localctx).aop = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__15 || _la==T__17) ) {
					((ExpContext)_localctx).aop = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(89);
				((ExpContext)_localctx).right = exp();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public FactorContext left;
		public Token aop;
		public TermContext right;
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			((TermContext)_localctx).left = factor();
			setState(95);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				{
				setState(93);
				((TermContext)_localctx).aop = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__18) | (1L << T__19))) != 0)) ) {
					((TermContext)_localctx).aop = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(94);
				((TermContext)_localctx).right = term();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
	 
		public FactorContext() { }
		public void copyFrom(FactorContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RelationalFactorContext extends FactorContext {
		public ValueContext left;
		public Token rop;
		public ValueContext right;
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public TerminalNode ROP() { return getToken(UFLParser.ROP, 0); }
		public RelationalFactorContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitRelationalFactor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BooleanFactorContext extends FactorContext {
		public ValueContext left;
		public Token bop;
		public ValueContext right;
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public TerminalNode BOP() { return getToken(UFLParser.BOP, 0); }
		public BooleanFactorContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitBooleanFactor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_factor);
		try {
			setState(107);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				_localctx = new RelationalFactorContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(97);
				((RelationalFactorContext)_localctx).left = value();
				setState(100);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
				case 1:
					{
					setState(98);
					((RelationalFactorContext)_localctx).rop = match(ROP);
					setState(99);
					((RelationalFactorContext)_localctx).right = value();
					}
					break;
				}
				}
				break;
			case 2:
				_localctx = new BooleanFactorContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(102);
				((BooleanFactorContext)_localctx).left = value();
				setState(105);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
				case 1:
					{
					setState(103);
					((BooleanFactorContext)_localctx).bop = match(BOP);
					setState(104);
					((BooleanFactorContext)_localctx).right = value();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	 
		public ValueContext() { }
		public void copyFrom(ValueContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExpressionValueContext extends ValueContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ExpressionValueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitExpressionValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionCallValueContext extends ValueContext {
		public TerminalNode ID() { return getToken(UFLParser.ID, 0); }
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public FunctionCallValueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitFunctionCallValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableValueContext extends ValueContext {
		public TerminalNode ID() { return getToken(UFLParser.ID, 0); }
		public VariableValueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitVariableValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntegerValueContext extends ValueContext {
		public TerminalNode INTEGER() { return getToken(UFLParser.INTEGER, 0); }
		public IntegerValueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitIntegerValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BooleanValueContext extends ValueContext {
		public BooleanValueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UFLVisitor ) return ((UFLVisitor<? extends T>)visitor).visitBooleanValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_value);
		int _la;
		try {
			setState(129);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				_localctx = new IntegerValueContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(109);
				match(INTEGER);
				}
				break;
			case 2:
				_localctx = new BooleanValueContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(110);
				_la = _input.LA(1);
				if ( !(_la==T__20 || _la==T__21) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 3:
				_localctx = new ExpressionValueContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(111);
				match(T__7);
				setState(112);
				exp();
				setState(113);
				match(T__9);
				}
				break;
			case 4:
				_localctx = new VariableValueContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(115);
				match(ID);
				}
				break;
			case 5:
				_localctx = new FunctionCallValueContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(116);
				match(ID);
				setState(117);
				match(T__7);
				setState(126);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__7) | (1L << T__15) | (1L << T__16) | (1L << T__20) | (1L << T__21) | (1L << INTEGER) | (1L << ID))) != 0)) {
					{
					setState(118);
					exp();
					setState(123);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__8) {
						{
						{
						setState(119);
						match(T__8);
						setState(120);
						exp();
						}
						}
						setState(125);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(128);
				match(T__9);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 3:
			return augexp_sempred((AugexpContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean augexp_sempred(AugexpContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		case 1:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3 \u0086\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3\37\n\3\f\3\16\3\"\13\3\3\3\3\3\5"+
		"\3&\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3/\n\3\f\3\16\3\62\13\3\5\3\64\n"+
		"\3\3\3\3\3\3\3\3\3\5\3:\n\3\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\7\5L\n\5\f\5\16\5O\13\5\7\5Q\n\5\f\5\16\5T\13\5"+
		"\3\6\3\6\5\6X\n\6\3\6\3\6\3\6\5\6]\n\6\3\7\3\7\3\7\5\7b\n\7\3\b\3\b\3"+
		"\b\5\bg\n\b\3\b\3\b\3\b\5\bl\n\b\5\bn\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\7\t|\n\t\f\t\16\t\177\13\t\5\t\u0081\n\t\3\t\5\t"+
		"\u0084\n\t\3\t\2\3\b\n\2\4\6\b\n\f\16\20\2\6\3\2\r\16\4\2\22\22\24\24"+
		"\4\2\7\7\25\26\3\2\27\30\u0092\2\22\3\2\2\2\49\3\2\2\2\6;\3\2\2\2\b=\3"+
		"\2\2\2\nW\3\2\2\2\f^\3\2\2\2\16m\3\2\2\2\20\u0083\3\2\2\2\22\23\7\3\2"+
		"\2\23\24\5\b\5\2\24\3\3\2\2\2\25\26\7\34\2\2\26\27\7\4\2\2\27:\5\b\5\2"+
		"\30\31\7\5\2\2\31\32\7\34\2\2\32%\7\6\2\2\33 \5\6\4\2\34\35\7\7\2\2\35"+
		"\37\5\6\4\2\36\34\3\2\2\2\37\"\3\2\2\2 \36\3\2\2\2 !\3\2\2\2!#\3\2\2\2"+
		"\" \3\2\2\2#$\7\b\2\2$&\3\2\2\2%\33\3\2\2\2%&\3\2\2\2&\'\3\2\2\2\'(\5"+
		"\6\4\2()\7\t\2\2)*\7\34\2\2*\63\7\n\2\2+\60\7\34\2\2,-\7\13\2\2-/\7\34"+
		"\2\2.,\3\2\2\2/\62\3\2\2\2\60.\3\2\2\2\60\61\3\2\2\2\61\64\3\2\2\2\62"+
		"\60\3\2\2\2\63+\3\2\2\2\63\64\3\2\2\2\64\65\3\2\2\2\65\66\7\f\2\2\66\67"+
		"\7\4\2\2\678\5\b\5\28:\3\2\2\29\25\3\2\2\29\30\3\2\2\2:\5\3\2\2\2;<\t"+
		"\2\2\2<\7\3\2\2\2=>\b\5\1\2>?\5\n\6\2?R\3\2\2\2@A\f\4\2\2AB\7\20\2\2B"+
		"C\5\b\5\2CD\7\21\2\2DE\5\b\5\5EQ\3\2\2\2FG\f\5\2\2GH\7\17\2\2HM\5\4\3"+
		"\2IJ\7\13\2\2JL\5\4\3\2KI\3\2\2\2LO\3\2\2\2MK\3\2\2\2MN\3\2\2\2NQ\3\2"+
		"\2\2OM\3\2\2\2P@\3\2\2\2PF\3\2\2\2QT\3\2\2\2RP\3\2\2\2RS\3\2\2\2S\t\3"+
		"\2\2\2TR\3\2\2\2UX\7\22\2\2VX\7\23\2\2WU\3\2\2\2WV\3\2\2\2WX\3\2\2\2X"+
		"Y\3\2\2\2Y\\\5\f\7\2Z[\t\3\2\2[]\5\n\6\2\\Z\3\2\2\2\\]\3\2\2\2]\13\3\2"+
		"\2\2^a\5\16\b\2_`\t\4\2\2`b\5\f\7\2a_\3\2\2\2ab\3\2\2\2b\r\3\2\2\2cf\5"+
		"\20\t\2de\7\31\2\2eg\5\20\t\2fd\3\2\2\2fg\3\2\2\2gn\3\2\2\2hk\5\20\t\2"+
		"ij\7\32\2\2jl\5\20\t\2ki\3\2\2\2kl\3\2\2\2ln\3\2\2\2mc\3\2\2\2mh\3\2\2"+
		"\2n\17\3\2\2\2o\u0084\7\33\2\2p\u0084\t\5\2\2qr\7\n\2\2rs\5\n\6\2st\7"+
		"\f\2\2t\u0084\3\2\2\2u\u0084\7\34\2\2vw\7\34\2\2w\u0080\7\n\2\2x}\5\n"+
		"\6\2yz\7\13\2\2z|\5\n\6\2{y\3\2\2\2|\177\3\2\2\2}{\3\2\2\2}~\3\2\2\2~"+
		"\u0081\3\2\2\2\177}\3\2\2\2\u0080x\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082"+
		"\3\2\2\2\u0082\u0084\7\f\2\2\u0083o\3\2\2\2\u0083p\3\2\2\2\u0083q\3\2"+
		"\2\2\u0083u\3\2\2\2\u0083v\3\2\2\2\u0084\21\3\2\2\2\23 %\60\639MPRW\\"+
		"afkm}\u0080\u0083";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}