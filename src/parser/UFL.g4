grammar UFL;

// PARSER RULES

program		: 'compute' augexp;

declaration	: ID '=' augexp	#variableDeclaration
			| 'function' functionId=ID ':' (type ('*' type)* '->')? codomain=type
				'such that' functionId2=ID '(' (ID (',' ID)*)? ')' '=' augexp		#functionDeclaration;

type		: 'int'
			| 'bool';
		
augexp		: augexp 'where' declaration (',' declaration)*						#boundExpression
			| thenExp=augexp 'if' condition=augexp 'else' elseExp=augexp		#conditionalExpression
			| exp																#simpleExpression;	
		
exp    		: (minus='-'|negation='!')? left=term (aop=('+' | '-') right=exp)?;
 
term   		: left=factor (aop=('*' | '/' | '%') right=term)? ;  

factor 		: left=value (rop=ROP right=value)?    		#relationalFactor
	        | left=value (bop=BOP right=value)? 		#booleanFactor; 
	        
value  		: INTEGER						#integerValue
     		| ( 'true' | 'false' )			#booleanValue
      		| '(' exp ')' 					#expressionValue
      		| ID  							#variableValue
      		| ID '(' (exp (',' exp)* )? ')'	#functionCallValue;
    
// LEXER RULES

ROP     : '==' | '>' | '<' | '<=' | '>=' | '!=' ;
BOP		: '&&' | '||' ;

//Numbers
fragment DIGIT : '0'..'9';    
INTEGER       : DIGIT+;

//IDs
fragment CHAR  : 'a'..'z' |'A'..'Z' ;
ID              : CHAR (CHAR | DIGIT)* ;

//ESCAPE SEQUENCES
WS              : (' '|'\t'|'\n'|'\r')-> skip ;
LINECOMENTS    	: '//' (~('\n'|'\r'))* -> skip ;
BLOCKCOMENTS    : '/*'( ~('/'|'*')|'/'~'*'|'*'~'/'|BLOCKCOMENTS)* '*/' -> skip ;

ERR     	: .  -> channel(HIDDEN) ; 
