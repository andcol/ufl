package parser;

import java.util.ArrayList;
import java.util.List;

import ast.BooleanFactor;
import ast.AugmentedExpression;
import ast.BooleanValue;
import ast.BoundExpression;
import ast.ConditionalExpression;
import ast.Declaration;
import ast.Expression;
import ast.ExpressionValue;
import ast.Factor;
import ast.FunctionCallValue;
import ast.FunctionDeclaration;
import ast.IntegerValue;
import ast.Node;
import ast.Program;
import ast.RelationalFactor;
import ast.SimpleExpression;
import ast.Term;
import ast.Value;
import ast.VariableDeclaration;
import ast.VariableValue;
import environment.Type;
import parser.UFLParser.BooleanFactorContext;
import parser.UFLParser.BooleanValueContext;
import parser.UFLParser.BoundExpressionContext;
import parser.UFLParser.ConditionalExpressionContext;
import parser.UFLParser.DeclarationContext;
import parser.UFLParser.ExpContext;
import parser.UFLParser.ExpressionValueContext;
import parser.UFLParser.FunctionCallValueContext;
import parser.UFLParser.FunctionDeclarationContext;
import parser.UFLParser.IntegerValueContext;
import parser.UFLParser.ProgramContext;
import parser.UFLParser.RelationalFactorContext;
import parser.UFLParser.SimpleExpressionContext;
import parser.UFLParser.TermContext;
import parser.UFLParser.TypeContext;
import parser.UFLParser.VariableDeclarationContext;
import parser.UFLParser.VariableValueContext;
import util.ArithmeticOperator;
import util.BooleanOperator;
import util.RelationalOperator;

public class UFLTreeGeneratingVisitor extends UFLBaseVisitor<Node> {

	@Override
	public Node visitProgram(ProgramContext ctx) {
		AugmentedExpression mainExpression = (AugmentedExpression)visit(ctx.augexp());
		return new Program(ctx.getStart().getLine(), mainExpression);
	}

	@Override
	public Node visitVariableDeclaration(VariableDeclarationContext ctx) {
		String id = ctx.ID().getText();
		AugmentedExpression expression = (AugmentedExpression)visit(ctx.augexp());
		return new VariableDeclaration(ctx.getStart().getLine(), id, expression);
	}

	@Override
	public Node visitFunctionDeclaration(FunctionDeclarationContext ctx) {
		String id = ctx.functionId.getText();
		List<Type> domain = new ArrayList<Type>();
		for(int i = 0; i < ctx.type().size()-1; i++) {
			domain.add(Type.fromString(ctx.type().get(i).getText()));
		}
		Type codomain = Type.fromString(ctx.codomain.getText());
		String id2 = ctx.functionId2.getText();
		List<String> parameters = new ArrayList<String>();
		for(int i = 2; i < ctx.ID().size(); i++) {
			parameters.add(ctx.ID().get(i).getText());
		}
		AugmentedExpression body = (AugmentedExpression) visit(ctx.augexp());
		return new FunctionDeclaration(ctx.getStart().getLine(), id, domain, codomain, id2,	parameters,	body);
	}

	@Override
	public Node visitBoundExpression(BoundExpressionContext ctx) {
		AugmentedExpression expression = (AugmentedExpression) visit(ctx.augexp());
		List<Declaration> declarations = new ArrayList<Declaration>();
		for(DeclarationContext d : ctx.declaration()) {
			declarations.add((Declaration) visit(d));
		}
		return new BoundExpression(ctx.getStart().getLine(), expression, declarations);
	}

	@Override
	public Node visitConditionalExpression(ConditionalExpressionContext ctx) {
		AugmentedExpression thenExpression = (AugmentedExpression) visit(ctx.thenExp);
		AugmentedExpression condition = (AugmentedExpression) visit(ctx.condition);
		AugmentedExpression elseExpression = (AugmentedExpression) visit(ctx.elseExp);
		
		return new ConditionalExpression(ctx.getStart().getLine(), thenExpression, condition, elseExpression);
	}

	@Override
	public Node visitSimpleExpression(SimpleExpressionContext ctx) {
		Expression expression = (Expression) visit(ctx.exp());
		return new SimpleExpression(ctx.getStart().getLine(), expression);
	}

	@Override
	public Node visitExp(ExpContext ctx) {
		boolean negative = ctx.minus != null;
		boolean negated = ctx.negation != null;
		Term term = (Term) visit(ctx.term());
		ArithmeticOperator aop = ctx.aop != null ? ArithmeticOperator.fromString(ctx.aop.getText()) : null;
		Expression expression = ctx.exp() != null ? (Expression) visit(ctx.exp()) : null;
		
		return new Expression(ctx.getStart().getLine(), negative, negated, term, aop, expression);
	}

	@Override
	public Node visitTerm(TermContext ctx) {
		Factor factor = (Factor) visit(ctx.factor());
		ArithmeticOperator aop = ctx.aop != null ? ArithmeticOperator.fromString(ctx.aop.getText()) : null;
		Term term = ctx.term() != null ? (Term) visit(ctx.term()) : null;
		
		return new Term(ctx.getStart().getLine(), factor, aop, term);
	}

	@Override
	public Node visitRelationalFactor(RelationalFactorContext ctx) {
		Value left = (Value) visit(ctx.left);
		RelationalOperator rop = ctx.rop != null ? RelationalOperator.fromString(ctx.rop.getText()) : null;
		Value right = ctx.right != null ? (Value) visit(ctx.right) : null;
		
		return new RelationalFactor(ctx.getStart().getLine(), left, right, rop);
	}

	@Override
	public Node visitBooleanFactor(BooleanFactorContext ctx) {
		Value left = (Value) visit(ctx.left);
		BooleanOperator bop = ctx.bop != null ? BooleanOperator.fromString(ctx.bop.getText()) : null;
		Value right = ctx.right != null ? (Value) visit(ctx.right) : null;
		
		return new BooleanFactor(ctx.getStart().getLine(), left, right, bop);
	}

	@Override
	public Node visitIntegerValue(IntegerValueContext ctx) {
		int integer = Integer.parseInt(ctx.getText());
		return new IntegerValue(ctx.getStart().getLine(), integer);
	}

	@Override
	public Node visitBooleanValue(BooleanValueContext ctx) {
		boolean truthValue = ctx.getText().equals("true") ? true : false;
		return new BooleanValue(ctx.getStart().getLine(), truthValue);
	}

	@Override
	public Node visitExpressionValue(ExpressionValueContext ctx) {
		Expression expression = (Expression) visit(ctx.exp());
		return new ExpressionValue(ctx.getStart().getLine(), expression);
	}

	@Override
	public Node visitVariableValue(VariableValueContext ctx) {
		String id = ctx.getText();
		return new VariableValue(ctx.getStart().getLine(), id);
	}

	@Override
	public Node visitFunctionCallValue(FunctionCallValueContext ctx) {
		String id = ctx.ID().getText();
		List<Expression> arguments = new ArrayList<Expression>();
		for(ExpContext e : ctx.exp()) {
			arguments.add((Expression) visit(e));
		}
		
		return new FunctionCallValue(ctx.getStart().getLine(), id, arguments);
	}

		
}
