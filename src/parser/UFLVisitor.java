// Generated from UFL.g4 by ANTLR 4.6
package parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link UFLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface UFLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link UFLParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(UFLParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variableDeclaration}
	 * labeled alternative in {@link UFLParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaration(UFLParser.VariableDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionDeclaration}
	 * labeled alternative in {@link UFLParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDeclaration(UFLParser.FunctionDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link UFLParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(UFLParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boundExpression}
	 * labeled alternative in {@link UFLParser#augexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoundExpression(UFLParser.BoundExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code conditionalExpression}
	 * labeled alternative in {@link UFLParser#augexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionalExpression(UFLParser.ConditionalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code simpleExpression}
	 * labeled alternative in {@link UFLParser#augexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleExpression(UFLParser.SimpleExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link UFLParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp(UFLParser.ExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link UFLParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(UFLParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relationalFactor}
	 * labeled alternative in {@link UFLParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalFactor(UFLParser.RelationalFactorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanFactor}
	 * labeled alternative in {@link UFLParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanFactor(UFLParser.BooleanFactorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code integerValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerValue(UFLParser.IntegerValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanValue(UFLParser.BooleanValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expressionValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionValue(UFLParser.ExpressionValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variableValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableValue(UFLParser.VariableValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionCallValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCallValue(UFLParser.FunctionCallValueContext ctx);
}