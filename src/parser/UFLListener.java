// Generated from UFL.g4 by ANTLR 4.6
package parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link UFLParser}.
 */
public interface UFLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link UFLParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(UFLParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link UFLParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(UFLParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variableDeclaration}
	 * labeled alternative in {@link UFLParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(UFLParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableDeclaration}
	 * labeled alternative in {@link UFLParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(UFLParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionDeclaration}
	 * labeled alternative in {@link UFLParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(UFLParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionDeclaration}
	 * labeled alternative in {@link UFLParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(UFLParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link UFLParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(UFLParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link UFLParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(UFLParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boundExpression}
	 * labeled alternative in {@link UFLParser#augexp}.
	 * @param ctx the parse tree
	 */
	void enterBoundExpression(UFLParser.BoundExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boundExpression}
	 * labeled alternative in {@link UFLParser#augexp}.
	 * @param ctx the parse tree
	 */
	void exitBoundExpression(UFLParser.BoundExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code conditionalExpression}
	 * labeled alternative in {@link UFLParser#augexp}.
	 * @param ctx the parse tree
	 */
	void enterConditionalExpression(UFLParser.ConditionalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code conditionalExpression}
	 * labeled alternative in {@link UFLParser#augexp}.
	 * @param ctx the parse tree
	 */
	void exitConditionalExpression(UFLParser.ConditionalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link UFLParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterExp(UFLParser.ExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link UFLParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitExp(UFLParser.ExpContext ctx);
	/**
	 * Enter a parse tree produced by {@link UFLParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(UFLParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link UFLParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(UFLParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relationalFactor}
	 * labeled alternative in {@link UFLParser#factor}.
	 * @param ctx the parse tree
	 */
	void enterRelationalFactor(UFLParser.RelationalFactorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relationalFactor}
	 * labeled alternative in {@link UFLParser#factor}.
	 * @param ctx the parse tree
	 */
	void exitRelationalFactor(UFLParser.RelationalFactorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code booleanFactor}
	 * labeled alternative in {@link UFLParser#factor}.
	 * @param ctx the parse tree
	 */
	void enterBooleanFactor(UFLParser.BooleanFactorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code booleanFactor}
	 * labeled alternative in {@link UFLParser#factor}.
	 * @param ctx the parse tree
	 */
	void exitBooleanFactor(UFLParser.BooleanFactorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code integerValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void enterIntegerValue(UFLParser.IntegerValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code integerValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void exitIntegerValue(UFLParser.IntegerValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code booleanValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void enterBooleanValue(UFLParser.BooleanValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code booleanValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void exitBooleanValue(UFLParser.BooleanValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expressionValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void enterExpressionValue(UFLParser.ExpressionValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expressionValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void exitExpressionValue(UFLParser.ExpressionValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variableValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void enterVariableValue(UFLParser.VariableValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void exitVariableValue(UFLParser.VariableValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionCallValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCallValue(UFLParser.FunctionCallValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionCallValue}
	 * labeled alternative in {@link UFLParser#value}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCallValue(UFLParser.FunctionCallValueContext ctx);
}