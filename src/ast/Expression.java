package ast;

import java.util.Arrays;
import java.util.List;

import environment.Environment;
import environment.Type;
import util.ArithmeticOperator;
import util.Error;
import util.Error.ErrCode;

public class Expression extends Node {

	private boolean negative;
	private boolean negated;
	
	private Term term;
	private ArithmeticOperator aop;
	private Expression expression;

	

	public Expression(int lineNumber, boolean negative, boolean negated, Term term, ArithmeticOperator aop, Expression expression) {
		super(lineNumber);
		this.negative = negative;
		this.negated = negated;
		this.term = term;
		this.aop = aop;
		this.expression = expression;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = term.checkSemantics(env);
		if (expression != null) {
			semanticErrors.addAll(expression.checkSemantics(env));
		}
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = term.checkType(env);
		Type leftType = term.getType();
		if(negative && leftType != Type.INT) {
			typeErrors.add(new Error(ErrCode.MINUS_TYPE_ERROR, lineNumber, Arrays.asList(leftType.toString())));
			leftType = Type.UNDEFINED;
		}
		if(negated && leftType != Type.BOOL) {
			typeErrors.add(new Error(ErrCode.NOT_TYPE_ERROR, lineNumber, Arrays.asList(leftType.toString())));
			leftType = Type.UNDEFINED;
		}
		type = leftType;
		
		if(expression != null) {
			typeErrors.addAll(expression.checkType(env));
			Type rightType = expression.getType();
			
			if(leftType == Type.UNDEFINED || rightType == Type.UNDEFINED) {
				type = Type.UNDEFINED;
			} else if (leftType != Type.INT || rightType != Type.INT) {
				type = Type.UNDEFINED;
				typeErrors.add(new Error(ErrCode.AOP_TYPE_ERROR, lineNumber, Arrays.asList(aop.toString(),leftType.toString(),rightType.toString())));
			} else {
				type = Type.INT;
			}
			
		} 

		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		String code = term.generateCode(env);
		if (negative) {
			code += "csig $a0 $a0\n";
		}
		if (negated) {
			code += "not $a0 $a0\n";
		}
		if (expression != null) {
			code += "push $a0\n"
					+ expression.generateCode(env)
					+ "top $t1\n"
					+ "pop\n"
					+ aop.toInstruction() + " $a0 $t1 $a0\n";
		}
		
		return code;
	}
	
}
