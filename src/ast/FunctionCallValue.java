package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import util.Error;
import util.Error.ErrCode;
import environment.Environment;
import environment.FunctionSymbolTableEntry;
import environment.Type;

public class FunctionCallValue extends Value {
	
	private String functionId;
	private List<Expression> arguments;
	
	public FunctionCallValue(int lineNumber, String functionId, List<Expression> arguments) {
		super(lineNumber);
		this.functionId = functionId;
		this.arguments = arguments;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = new ArrayList<Error>();
		FunctionSymbolTableEntry fste = env.retrieveFunction(functionId);
		if(fste == null) {
			semanticErrors.add(new Error(ErrCode.UNDEF_FUN, lineNumber, Arrays.asList(functionId)));
		} else if(fste.getDomain().size() != arguments.size()) {
			semanticErrors.add(new Error(ErrCode.ARG_NUM_MISM, lineNumber, Arrays.asList(Integer.toString(fste.getDomain().size()),Integer.toString(arguments.size()))));
		}
		for(Expression arg : arguments) {
			semanticErrors.addAll(arg.checkSemantics(env));
		}
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = new ArrayList<Error>();
		FunctionSymbolTableEntry fste = env.retrieveFunction(functionId);
		List<Type> domain = fste.getDomain();
		Type codomain = fste.getCodomain();
		type = codomain;
		for(Expression arg : arguments) {
			typeErrors.addAll(arg.checkType(env));
			if(arg.getType() == Type.UNDEFINED) {
				type = Type.UNDEFINED;
			}
		}
		for(int i = 0; i < domain.size(); i++) {
			if(domain.get(i) != arguments.get(i).getType()) {
				type = Type.UNDEFINED;
				typeErrors.add(new Error(ErrCode.ARG_TYPE_MISM, lineNumber, Arrays.asList(domain.get(i).toString(),arguments.get(i).getType().toString())));
			}
		}
		
		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		String functionLabel = env.retrieveFunction(functionId).getLabel();
		String code = "push $fp\n";
		//Push arguments
		for(int i = arguments.size()-1; i >= 0; i--) {
			code += arguments.get(i).generateCode(env);
			code += "push $a0\n";
		}
		//Push AL
		code += "lw $al 0($fp)\n";
		for(int i = 1; i < env.getRelativeDepth(functionId); i++) {
			code += "lw $al 0($al)\n";
		}
		code += "push $al\n"
				+ "jal " + functionLabel + "\n";	//Jump to function label saving RA
		
		return code;
	}
	
	
}
