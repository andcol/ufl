package ast;

import java.util.List;
import util.Error;
import environment.Environment;

public class ExpressionValue extends Value{
	
	private Expression expression;

	public ExpressionValue(int lineNumber, Expression expression) {
		super(lineNumber);
		this.expression = expression;
	}	

	@Override
	public List<Error> checkSemantics(Environment env) {
		return expression.checkSemantics(env);
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = expression.checkType(env);
		
		type = expression.getType();
		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		return expression.generateCode(env);
	}
	
	
}
