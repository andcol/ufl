package ast;

import java.util.ArrayList;
import util.Error;
import java.util.List;

import environment.Environment;
import environment.Type;

public class IntegerValue extends Value {

	private int integer;

	public IntegerValue(int lineNumber, int integer) {
		super(lineNumber);
		this.integer = integer;
	}
	
	@Override
	public List<Error> checkSemantics(Environment env) {
		return new ArrayList<Error>();
	}

	@Override
	public List<Error> checkType(Environment env) {
		type = Type.INT;
		return new ArrayList<Error>();
	}

	@Override
	public String generateCode(Environment env) {
		return "li $a0 " + integer + "\n";
	}
	
	
}
