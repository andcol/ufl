package ast;

import java.util.ArrayList;
import util.Error;
import java.util.List;

import environment.Environment;

public class BoundExpression extends AugmentedExpression {

	private AugmentedExpression expression;
	private List<Declaration> bindings;
	
	public BoundExpression(int lineNumber, AugmentedExpression expression, List<Declaration> bindings) {
		super(lineNumber);
		this.expression = expression;
		this.bindings = bindings;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = new ArrayList<Error>();
		env.openScope();
		for(Declaration d : bindings) {
			//This adds bindings to the symbol table
			semanticErrors.addAll(d.checkSemantics(env));
		}
		semanticErrors.addAll(expression.checkSemantics(env));
		env.closeScope();
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = new ArrayList<Error>();
		env.openScope();
		for(Declaration d : bindings) {
			//This adds bindings to the symbol table
			typeErrors.addAll(d.checkType(env));
		}
		typeErrors.addAll(expression.checkType(env));
		env.closeScope();
		
		type = expression.getType();
		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		String code = "";
		for(Declaration d : bindings) {
			code += d.generateCode(env);
		}
		code += expression.generateCode(env);
		
		return code;
	}
	
	

}
