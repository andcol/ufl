package ast;

import java.util.Arrays;
import java.util.List;
import util.Error;
import environment.Environment;
import environment.Type;
import util.RelationalOperator;
import util.Error.ErrCode;

public class RelationalFactor extends Factor {

	private RelationalOperator rop;

	public RelationalFactor(int lineNumber, Value left, Value right, RelationalOperator rop) {
		super(lineNumber, left, right);
		this.rop = rop;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = left.checkType(env);
		Type leftType = left.getType();
		type = leftType;
		if(right != null) {
			typeErrors.addAll(right.checkType(env));
			Type rightType = right.getType();
			
			if(leftType == Type.UNDEFINED || rightType == Type.UNDEFINED) {
				type = Type.UNDEFINED;
			} else if (leftType != rightType) {
				type = Type.UNDEFINED;
				typeErrors.add(new Error(ErrCode.ROP_TYPE_MISM, lineNumber, Arrays.asList(rop.toString(),leftType.toString(),rightType.toString())));
			} else {
				type = Type.BOOL;
			}
			
		} 

		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		String code = left.generateCode(env);
		if (right != null) {
			code += "push $a0\n"
					+ right.generateCode(env)
					+ "top $t1\n"
					+ "pop\n"
					+ rop.toInstructions(env.newLabel(), env.newLabel());
		}
		return code;
	}
	
	

}
