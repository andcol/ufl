package ast;

import java.util.Arrays;
import java.util.List;
import util.Error;
import util.Error.ErrCode;
import environment.Environment;
import environment.Type;
import util.ArithmeticOperator;

public class Term extends Node {
	
	private Factor factor;
	private ArithmeticOperator aop;
	private Term term;
	

	public Term(int lineNumber, Factor factor, ArithmeticOperator aop, Term term) {
		super(lineNumber);
		this.factor = factor;
		this.aop = aop;
		this.term = term;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = factor.checkSemantics(env);
		if (term != null) {
			semanticErrors.addAll(term.checkSemantics(env));
		}
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = factor.checkType(env);
		Type leftType = factor.getType();
		type = leftType;
		if(term != null) {
			typeErrors.addAll(term.checkType(env));
			Type rightType = term.getType();
			
			if(leftType == Type.UNDEFINED || rightType == Type.UNDEFINED) {
				type = Type.UNDEFINED;
			} else if (leftType != Type.INT || rightType != Type.INT) {
				type = Type.UNDEFINED;
				typeErrors.add(new Error(ErrCode.AOP_TYPE_ERROR, lineNumber, Arrays.asList(aop.toString(),leftType.toString(),rightType.toString())));
			} else {
				type = Type.INT;
			}
			
		} 

		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		String code = factor.generateCode(env);
		if (term != null) {
			code += "push $a0\n"
					+ term.generateCode(env)
					+ "top $t1\n"
					+ "pop\n"
					+ aop.toInstruction() + " $a0 $t1 $a0\n";
		}
		
		return code;
	}

}
