package ast;

import java.util.List;
import util.Error;
import environment.Environment;

public class Program extends Node {
	
	private AugmentedExpression mainExpression;

	public Program(int lineNumber, AugmentedExpression mainExpression) {
		super(lineNumber);
		this.mainExpression = mainExpression;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		env.openScope();
		List<Error> semanticErrors = mainExpression.checkSemantics(env);
		env.closeScope();
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		env.openScope();
		List<Error> typeErrors = mainExpression.checkType(env);
		type = mainExpression.getType();
		env.closeScope();
		
		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		env.openScope();
		String code = "push $a0\n"
						+ "push $a0\n"
						+ mainExpression.generateCode(env)
						+ "print $a0\n";
		env.closeScope();
		
		return code;
	}

}
