package ast;

import java.util.ArrayList;
import util.Error;
import java.util.List;

import environment.Environment;
import environment.Type;

public class BooleanValue extends Value {

	private boolean truthValue;

	public BooleanValue(int lineNumber, boolean truthValue) {
		super(lineNumber);
		this.truthValue = truthValue;
	}
	
	@Override
	public List<Error> checkSemantics(Environment env) {
		return new ArrayList<Error>();
	}

	@Override
	public List<Error> checkType(Environment env) {
		type = Type.BOOL;
		return new ArrayList<Error>();
	}

	@Override
	public String generateCode(Environment env) {
		return "li $a0 " + (truthValue ? 1 : 0) + "\n";
	}
	
	
}
