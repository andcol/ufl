package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import util.Error;
import util.Error.ErrCode;
import environment.Environment;
import environment.VariableSymbolTableEntry;

public class VariableValue extends Value {
	
	private String variableId;

	public VariableValue(int lineNumber, String variableId) {
		super(lineNumber);
		this.variableId = variableId;
	}
	
	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = new ArrayList<Error>();
		if(env.retrieveVariable(variableId) == null) {
			semanticErrors.add(new Error(ErrCode.UNDEF_VAR, lineNumber, Arrays.asList(variableId)));
		}
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		VariableSymbolTableEntry entry = env.retrieveVariable(variableId);
		
		type = entry.getType();
		return new ArrayList<Error>();
	}

	@Override
	public String generateCode(Environment env) {
		int relativeDepth = env.getRelativeDepth(variableId);
		int offset = env.retrieveVariable(variableId).getOffset();
		String code = "";
		if (relativeDepth == 0) {
			//variable is local, access directly from local AR
			code += "lw $a0 " + offset + "($fp)\n";
		} else {
			//external variable, climb static chain
			code += "lw $al 0($fp)\n";
			for(int i = 1; i < relativeDepth; i++) {
				code += "lw $al 0($al)\n";
			}
			code += "lw $a0 " + offset + "($al)\n";
		}
		
		return code;
	}

}
