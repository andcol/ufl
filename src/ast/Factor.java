package ast;

import java.util.List;
import util.Error;
import environment.Environment;

public abstract class Factor extends Node {
	
	protected Value left;
	protected Value right;

	

	public Factor(int lineNumber, Value left, Value right) {
		super(lineNumber);
		this.left = left;
		this.right = right;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = left.checkSemantics(env);
		if (right != null) {
			semanticErrors.addAll(right.checkSemantics(env));
		}
		
		return semanticErrors;
	}
	
	

}
