package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import util.Error;
import util.Error.ErrCode;
import environment.Environment;
import environment.Type;

public class FunctionDeclaration extends Declaration {
	
	private String functionId;
	private List<Type> domain;
	private Type codomain;
	private String functionId2;
	private List<String> parameters;
	private AugmentedExpression body;
	
	public FunctionDeclaration(int lineNumber, String functionId, List<Type> domain, Type codomain, String functionId2,
			List<String> parameters, AugmentedExpression body) {
		super(lineNumber);
		this.functionId = functionId;
		this.domain = domain;
		this.codomain = codomain;
		this.functionId2 = functionId2;
		this.parameters = parameters;
		this.body = body;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = new ArrayList<Error>();
		if(!functionId.equals(functionId2)) {
			semanticErrors.add(new Error(ErrCode.NAME_MISM_FUN, lineNumber, Arrays.asList(functionId, functionId2)));
		}
		if(!env.addFunction(functionId, parameters, domain, codomain)) {
			semanticErrors.add(new Error(ErrCode.MDEF_FUN, lineNumber, Arrays.asList(functionId)));
		}
		if(parameters.size() != domain.size()) {
			semanticErrors.add(new Error(ErrCode.PARAM_NUM_MISM, lineNumber, Arrays.asList(Integer.toString(parameters.size()), Integer.toString(domain.size()))));
		}
		env.openScope();
		for(String pid : parameters) {
			if(!env.addVariable(pid, null)) {	//We do not need type now
				//This adds formal parameters to the symbol table
				semanticErrors.add(new Error(ErrCode.MDEF_PARAM, lineNumber, Arrays.asList(pid)));
			}
		}
		semanticErrors.addAll(body.checkSemantics(env));
		env.closeScope();
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = new ArrayList<Error>();
		env.addFunction(functionId, parameters, domain, codomain);
		type = Type.VOID;
		
		env.openScope();
		for(int i = 0; i < domain.size(); i++) {
			env.addVariable(parameters.get(i), domain.get(i));
		}
		typeErrors.addAll(body.checkType(env));
		env.closeScope();
		
		Type bodyType = body.getType();
		if(bodyType != codomain) {
			type = Type.UNDEFINED;
			typeErrors.add(new Error(ErrCode.BODY_TYPE_MISM, lineNumber, Arrays.asList(functionId, codomain.toString(), bodyType.toString())));
		}
		
		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		//Add function to the symbol table
		env.addFunction(functionId, parameters, domain, codomain);
		String functionLabel = env.retrieveFunction(functionId).getLabel();
				
		//Add parameters to the symbol table
		env.openScope();
		for(int i = 0; i < domain.size(); i++) {
			env.addParameter(parameters.get(i), i);
		}
				
		//Create skip label, used to avoid executing the declared code unless called
		String skipLabel = env.newLabel();
				
		String code = "b " + skipLabel + "\n"
						+ functionLabel + ":\n"
						+ "move $fp $sp\n"
						+ "push $ra\n"
						+ body.generateCode(env); //Generate body code
		
		//Function exit
		int localVars = env.getNumberOfLocalVariables();
		if (localVars > 0) {
			code += "addi $sp $sp " + (localVars-2) + "\n";	//Undo local variables (not RA or AL) on the stack
		}
		env.closeScope();
				
		code += "top $ra\n" //load RA
				+ "addi $sp $sp " + (parameters.size()+2) + "\n" //undo RA, AL and arguments on the stack 
				+ "top $fp\n"
				+ "pop\n"
				+ "jr $ra\n"
				+ skipLabel + ":\n";
				
				return code;
	}
	
	
}
