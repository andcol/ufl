package ast;

import java.util.Arrays;
import java.util.List;
import util.Error;
import util.Error.ErrCode;
import environment.Environment;
import environment.Type;

public class ConditionalExpression extends AugmentedExpression {
	
	private AugmentedExpression thenExpression;
	private AugmentedExpression conditionExpression;
	private AugmentedExpression elseExpression;
	
	public ConditionalExpression(int lineNumber, AugmentedExpression thenExpression, AugmentedExpression conditionExpression,
			AugmentedExpression elseExpression) {
		super(lineNumber);
		this.thenExpression = thenExpression;
		this.conditionExpression = conditionExpression;
		this.elseExpression = elseExpression;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = thenExpression.checkSemantics(env);
		semanticErrors.addAll(conditionExpression.checkSemantics(env));
		semanticErrors.addAll(elseExpression.checkSemantics(env));
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = thenExpression.checkType(env);
		typeErrors.addAll(conditionExpression.checkType(env));
		typeErrors.addAll(elseExpression.checkType(env));
		
		Type thenType = thenExpression.getType();
		Type conditionType = conditionExpression.getType();
		Type elseType = elseExpression.getType();
		
		if(thenType == Type.UNDEFINED || conditionType == Type.UNDEFINED || elseType == Type.UNDEFINED) {
			type = Type.UNDEFINED;
		} else {
			if(thenType != elseType) {
				type = Type.UNDEFINED;
				typeErrors.add(new Error(ErrCode.COND_TYPE_MISM, lineNumber, Arrays.asList(thenType.toString(), elseType.toString())));
			} else {
				type = thenType;
			}
			if(conditionType != Type.BOOL) {
				type = Type.UNDEFINED;
				typeErrors.add(new Error(ErrCode.COND_TYPE_ERROR, lineNumber, Arrays.asList(conditionType.toString())));
			}
		}
		
		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		String elseLabel = env.newLabel();
		String endLabel = env.newLabel();
		return conditionExpression.generateCode(env)
				+ "li $t1 0\n"
				+ "beq $t1 $a0 " + elseLabel + "\n"
				+ thenExpression.generateCode(env)
				+ "b " + endLabel + "\n"
				+ elseLabel + ":\n"
				+ elseExpression.generateCode(env)
				+ endLabel + ":\n";
	}
	
	
}
