package ast;

import java.util.Arrays;
import java.util.List;
import util.Error;
import util.Error.ErrCode;
import environment.Environment;
import environment.Type;
import util.BooleanOperator;

public class BooleanFactor extends Factor {

	private BooleanOperator bop;

	public BooleanFactor(int lineNumber, Value left, Value right, BooleanOperator bop) {
		super(lineNumber, left, right);
		this.bop = bop;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = left.checkType(env);
		Type leftType = left.getType();
		type = leftType;
		if(right != null) {
			typeErrors.addAll(right.checkType(env));
			Type rightType = right.getType();
			
			if(leftType == Type.UNDEFINED || rightType == Type.UNDEFINED) {
				type = Type.UNDEFINED;
			} else if (leftType != Type.BOOL || rightType != Type.BOOL) {
				type = Type.UNDEFINED;
				typeErrors.add(new Error(ErrCode.BOP_TYPE_ERROR, lineNumber, Arrays.asList(bop.toString(),leftType.toString(),rightType.toString())));
			} else {
				type = Type.BOOL;
			}
			
		} 

		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		String code = left.generateCode(env);
		if (right != null) {
			//use the fact that x && y = 0 if x==0, y otherwise (same with || and 1)
			String shortcircuitLabel = env.newLabel();
			code += "li $t1 " + bop.getAbsorbingElement() + "\n"
					+ "beq $t1 $a0 " + shortcircuitLabel + "\n"
					+ right.generateCode(env)
					+ shortcircuitLabel + ":\n";
		}
		return code;
	}
	
	

}
