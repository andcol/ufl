package ast;

import java.util.Arrays;
import java.util.List;

import environment.Environment;
import environment.Type;
import util.Error.ErrCode;
import util.Error;

public class VariableDeclaration extends Declaration {
	private String variableId;
	private AugmentedExpression expression;
	
	public VariableDeclaration(int lineNumber, String variableId, AugmentedExpression expression) {
		super(lineNumber);
		this.variableId = variableId;
		this.expression = expression;
	}

	@Override
	public List<Error> checkSemantics(Environment env) {
		List<Error> semanticErrors = expression.checkSemantics(env);
		if(!env.addVariable(variableId, type)) {
			semanticErrors.add(new Error(ErrCode.MDEF_VAR, lineNumber, Arrays.asList(variableId)));
		}
		
		return semanticErrors;
	}

	@Override
	public List<Error> checkType(Environment env) {
		List<Error> typeErrors = expression.checkType(env);
		env.addVariable(variableId, expression.getType());
		
		type = Type.VOID;		
		return typeErrors;
	}

	@Override
	public String generateCode(Environment env) {
		String code = expression.generateCode(env);
		env.addVariable(variableId, null);
		code += "push $a0\n"; //local variables grow on the stack
		return code;
	}
	
	
}
