package ast;

import java.util.List;
import environment.Environment;
import environment.Type;
import util.Error;

public abstract class Node {
	
	protected int lineNumber;
	protected Type type;
	
	public Node(int lineNumber) {
		this.lineNumber = lineNumber;
		this.type = null;
	}
	
	public int getLineNumber() {
		return lineNumber;
	}

	public Type getType() {
		return type;
	}

	public abstract List<Error> checkSemantics(Environment env);
	
	public abstract List<Error> checkType(Environment env);
	
	public abstract String generateCode(Environment env);

}
