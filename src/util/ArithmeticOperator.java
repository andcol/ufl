package util;

public enum ArithmeticOperator {
	PLUS{
		@Override
		public String toString() {
			return "+";
		}
		@Override
		public String toInstruction() {
			return "add";
		}
	},
	MINUS{
		@Override
		public String toString() {
			return "-";
		}
		@Override
		public String toInstruction() {
			return "sub";
		}
	},
	TIMES{
		@Override
		public String toString() {
			return "*";
		}
		@Override
		public String toInstruction() {
			return "mult";
		}
	},
	DIVIDE{
		@Override
		public String toString() {
			return "/";
		}
		@Override
		public String toInstruction() {
			return "div";
		}
	},
	MODULO{
		@Override
		public String toString() {
			return "%";
		}
		@Override
		public String toInstruction() {
			return "mod";
		}
	};
	
	public abstract String toInstruction();
	
	public static ArithmeticOperator fromString(String string) {
		if(string.equals("*")) {
			return TIMES;
		} else if(string.equals("/")) {
			return DIVIDE;
		} else if(string.equals("+")) {
			return PLUS;
		} else if(string.equals("-")) {
			return MINUS;
		} else if(string.contentEquals("%")) {
			return MODULO;
		}
		
		return null;
	}
}