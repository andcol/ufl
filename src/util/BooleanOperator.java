package util;

public enum BooleanOperator {
	AND {
		@Override
		public String toString() {
			return "&&";
		}
		@Override
		public String getAbsorbingElement() {
			return "0";
		}
	},
	OR {
		@Override
		public String toString() {
			return "||";
		}
		@Override
		public String getAbsorbingElement() {
			return "1";
		}
	};

	public abstract String getAbsorbingElement();
	
	public static BooleanOperator fromString(String string) {
		if(string.equals("&&")) {
			return AND;
		} else if (string.equals("||")) {
			return OR;
		}
		
		return null;
	}
}
