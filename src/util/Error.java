package util;

import java.util.List;

import environment.Type;

public class Error {
	public enum ErrCode {
		UNDEF_VAR,
		UNDEF_FUN,
		MDEF_VAR,
		MDEF_FUN,
		NAME_MISM_FUN,
		PARAM_NUM_MISM,
		MDEF_PARAM,
		ARG_NUM_MISM,
		BOP_TYPE_ERROR,
		ROP_TYPE_MISM,
		AOP_TYPE_ERROR,
		MINUS_TYPE_ERROR,
		NOT_TYPE_ERROR,
		COND_TYPE_ERROR,
		COND_TYPE_MISM,
		ARG_TYPE_MISM,
		BODY_TYPE_MISM,
	}
	
	private ErrCode code;
	private int line;
	private List<String> offendingEntities;
	
	public Error(ErrCode code, int line) {
		super();
		this.code = code;
		this.line = line;
	}

	public Error(ErrCode code, int line, List<String> offendingEntities) {
		super();
		this.code = code;
		this.line = line;
		this.offendingEntities = offendingEntities;
	}

	@Override
	public String toString() {
		return "Error [code=" + code + ", line=" + line + ", offendingEntities=" + offendingEntities + "]";
	}
	
	
}
