# Useless Functional Language (UFL) 

Have you ever dreamed of spending your summer holidays defining and implementing a small, useless functional programming language? Neither have I! But I loathe the beach and I had no internet connection, so here we are.

## Programs

An UFL program is always introduced by the keyword `compute`. The result of a computation is *always* printed at the end of the computation itself. A typical program has the following structure:

```
compute <exp> [where <declarations>]
```

where `exp` is an arithmetic or boolean expression and `where` introduces a(n optional) sequence of  comma-separated variable and function `<declarations>` to be taken into account when evaluating `exp`. A variable can be of type `int` or `bool` and it is declared by writing:

```
<name> = <exp>
```

where the type of the variable named `name` is inferred from `exp`. Functions do not support currying and they are defined in a slightly more complicated way:

```
function <name>: [<T1>] -> <T2>
	such sthat <name>([<comma-separated arguments>]) = <exp>
```

where `name` is the name of the function, `T1` is the type of the inputs (defined as a product type) or the unit type when omitted, and `T2` is the type of `exp` , i.e. the output type.

## Examples

The following program computes the n-th Fibonacci number in the naive, exponential-time way:

```
compute fib(n) where
	n = 36,	
	function fib:int->int
	such sthat fib(n) = n if n<=1 else fib(n-1) + fib(n-2)
```

